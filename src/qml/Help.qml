import QtQuick 2.0
import QtQuick.Controls 2.0
Label {
    text: "Simple color fill application. By selecting a color, the boad gets flood filled from top left. The goal is to make the board in one color in as few fills as possible."
    wrapMode: TextArea.Wrap
}
